# Setting up Gitlab account

## SSH key pair (login ECE551 server without typing password)

SSH, or secure shell, is a secure protocol and the most common way of safely administering remote servers. [Understanding the SSH Encryption and Connection Process](https://www.digitalocean.com/community/tutorials/understanding-the-ssh-encryption-and-connection-process).

Using SSH enables login in servers without password.

* generate SSH key pair
  * Open up the Terminal.
  * Type in the following command:
  * `$ ssh-keygen -t rsa`
  where `$` stands for shell promote.
  * Next your are asked to verify the saved path of rsa files (generated key pairs).
  `$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/yt114/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:`
  * press `Enter` twice to set SSH in default setting. For no-default setting, refer to [Working with non-default SSH key pair paths for](https://docs.gitlab.com/ee/ssh/#working-with-non-default-ssh-key-pair-paths).
  * Now you have generated your SSH key pairs. Have a look at `~/.ssh` directory:
  * `id_rsa` contains user private key. kept secret by the SSH user on his/her client machine. 
  * `id_rea.pub` contains public key to be registered on the server to allow authorization of the user on a server.
```bash
~/.ssh$ ls
id_rsa  id_rsa.pub
```
* setting up SSH login
  * For mac user:
  `pbcopy < ~/.ssh/id_rsa.pub`
   pbcopy,  pbpaste  -  provide copying and pasting to the pasteboard (the Clipboard) from command line (run `man pbcopy` in your terminal to see more details). 
   * Or open `~/.ssh/id_rsa.pub` with `emacs` and copy the content.
* SSH configure file
  * `emacs  ~/.ssh/config`
  * type in the following
```
Host <Hostname>
    HostName vcm-181.vm.duke.edu
    User <Netid>
```
* register SSH public key
  * Login to ECE551
  * run `emacs ~/.ssh/authorized_keys`
  * paste your public key into this file.
* now log out and type:
  `ssh Hostname`
  you should be able to login 551 Server without having to type passwd.

## Setting up your gitlab account
  * sign in duke lab: https://gitlab.oit.duke.edu/users/sign_in
![Gitlab login](/figures/gitlab_login.png "Image Title")
  * Find the homepage of this recitation:
https://gitlab.oit.duke.edu/yt114/ECE_551_Recitation
  * click icon on upper-right side, choose setting.
  * click profile on left sidebar. File in your account, emaile and etc.  
  * In account setting/SSH key pairs, paste your publick key and save. 

## clone this repository
* in your terminal run (# representing comment in bash language)
```
git config --global user.name “[firstname lastname]”
# set a name that is identifiable for credit when review version history
git config --global user.email “[valid-email]”
# set an email address that will be associated with each history marker
```
* in any directory, run
```
git init
# initialize an existing directory as a Git repository
git clone git@gitlab.oit.duke.edu:yt114/ECE_551_Recitation.git
```
