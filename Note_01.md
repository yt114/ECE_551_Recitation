# Topics of this Retation Notes includes:
* variable
* function
* variable scope
* loop
* compilation, Makefile 

## variable
* bits (short for binary digit): the smallest unit of data in a computer. A bit has a single binary value (0,1). 
* type: computer interpretation of a series of bits of certain size.

### basic data type in C
| Data Type|             Memory (bytes)|          Range|                      Format Specifier|
|----------|:-------------:|------:|------:|
| short int |                  2|          -32,768 to 32,767|                       %hd
| unsigned short int |         2|           0 to 65,535 |                           %hu
| unsigned int   |             4|           0 to 4,294,967,295   |                  %u
| int|                         4|          -2,147,483,648 to 2,147,483,647 |        %d
| long int|                    4|          -2,147,483,648 to 2,147,483,647|         %ld
| unsigned long int|           4|           0 to 4,294,967,295 |                    %lu
| long long int|               8|          -(2^63) to (2^63)-1 |                    %lld
| unsigned long long int|      8|           0 to 18,446,744,073,709,551,615 |       %llu
| signed char|                 1|          -128 to 127 |                            %c 
| unsigned char|               1|           0 to 255 |                              %c
| float|                       4|                   |                               %f
| double |                     8|                  |                              %l
| long double   |              12|                |                          %Lf

* Format Specifier: A format specifier is a sequence formed by an initial percentage sign (%) indicates a format specifier, which is used to specify the type and format of the data to be retrieved from the stream and stored into the locations pointed by the additional arguments 

Ex:
To print an interger to stdout,
```c
int num = 9;
printf("Value of num is = %i\n", num);
```


```
Output:
Value of num is = 9
```

## Difference between Python and C/C++ (Not examinable)
This note is based on
[A Transition Guide: Python to C++](https://pdfs.semanticscholar.org/9ad1/030685050e949d1a3d6d92bababcbe075e07.pdf)

No single language is perfect, and each strikes its own balance in
trying to support the development of efficient, maintainable, and reusable software.

### Interpreter versus Compiler
C/C++: compiled language
1. compile-time: compiler translates source code to machine code and generates another executable file.
2. run-time: directly run the the generated executable file and compiler is not needed in this phase.

Python: interpreted language
1. Python interpreter is actually run on machine. It interpretes each line of python code in order and translates them into machine code. 
2. Which means there is no need of main() function in python or any other interpreted language e.g. Bash script. Because interpreter evaluates/run every line in oreder. You can write a script names foo.py like,

```
a = 1
print a
```
then run it in terminal:

```
python3 foo.py
```

* C/C++ have shorter run time than python since there is no need to translate source code to machine code during running.
* C/C++: During compilation, if there are syntax errors, error are reported and the compilation fails. Python doesn't show the error untile the very exact line is interpreted. See Py_error.py in ./code_example.

## Dynamic versus Static Typing

C/C++: statically typed languages.

Python: dynamically typed language.
* NO explicit type declaration. 
* Let interpreter decide the type
  * how to know type: `type()` function.
* Define functions in Python:

```python

def foo(a, b):
  print(a)
  print(b)
  pass
```
# compiler

*source code -> prepreocessor-> compiler*
*source code -> prepreocessed source code-> assembely language-> object file -> executable file*

### preprocessor
enables:
1. header file.
2. macro expansion

* directives: directs preporcessor to include the head file/code. 
```
#include <stdlib.h>
#include "header.h"
```

Typically header files include:
* funciton prototype 
  ` int myfunction(int n); `
* macro definition
  `#define PI 3.14`
* type definition
`typedef` is a keyword used in C language to assign alternative names to existing datatypes

## compiler
1. assembely lanugae: lowest level human-readable laguage.
2. object file: .o files
`gcc -c example.c -o ex.o` 

__what is left?__
functions that is referenced from other files (library). Ex. `prinf()` function defined in `#include <stdio.h>`. 

3. linking 
produce actual executable code (`a.out`).

# To compile your code
using the following option:
```
gcc -o code.out -pedantic -std=gnu99 -Wall -Werror code.c
```
__why this is important?__
1. this is what grader does.
2. -pedantic: make sure your code is most standard c code.
> Issue all the warnings demanded by strict ISO C and ISO C ++ ; reject all programs that use forbidden extensions, and some other programs that do not follow ISO C and ISO C ++.
3. -Wall: It's short for "warn all"
> This enables all the warnings about constructions that some users consider questionable, and that are easy to avoid (or modify to prevent the warning).

__In short, -Wall and -pedantic raise as many as possible warnings that gcc can tell you about.__

4. -Werror
> Make all warnings into hard errors. Source code which triggers warnings will be rejected.

## Makefile
1. create a file named Makefile
`emacs Makefile`
2. The most simple file for assignment 19.
```bash
retirement: retirement.c
	gcc -o retirement -pedantic -std=gnu99 -Wall -Werror retirement.c
```
3. type make in shell
`$ make`
4. Using Macro which is like variables in c
```bash
CFLAGS=-pedantic -std=gnu99 -Wall -Werror
retirement: retirement.c
	gcc -o retirement $(CFLAGS) retirement.c
```
Two special characters:

$@ is the name of the file to be made.

$? is the names of the dependents.
```bash
CFLAGS=-pedantic -std=gnu99 -Wall -Werror
retirement: retirement.c
	gcc -o $@ $(CFLAGS) $?
```
5. make clean: graders asks for that
```
clean:
        -rm retirement
```
Then run in bash
```
make clean
```
## Other thing we haven't mentioned about
1. Defining Dependencies in Makefile.
2. phony object (make clean) and all other terminology.
3. Conditional Directives.
4. A lot of things going on (check Aop).






